# cecar-robot-engine

A [rosserial](http://wiki.ros.org/rosserial) client application on a [STM32F4-DISCOVERY](https://www.st.com/STM32F4-DISCOVERY) board.

It is designed to be IO compatible to https://gitlab.com/NeuroRace/neuroracer-robot-engine

The STM32 project was generated using STM32CubeMX 5.4.0.

The rosserial header files were generated in a catkin workspace using
 * https://github.com/ros-drivers/rosserial.git and
 * https://github.com/yoneken/rosserial_stm32.git
and manually copied into the STM32 project folder.

See rosserial_arduino tutorials for more information.


## CeCar robot engine (RCU) peripherals

- 8 MHz crystal HSE clock, 168 MHz HCLK
- **rosserial comm: USART2** 115200 8N1 (COM_RX PA2 COM_TX PA3) DMA
- **PWM out**
  - steering angle: TIM3 CH1 PWM - SERVO_PWM PC6
  - throttle (ESC): TIM3 CH2 PWM - ESC_PWM PB5
  - TIM3 is an APB2 peripheral 84 MHz timer clock
- ADC: ADC1 IN1 (PA1) IN9 (PB1) IN11 (PC1) IN12 (PC2)
- (serial debug: USART3 115200 8N1 (TERM_RX PB11 TERM_TX PD8) )
- wheel encoder (odometry): (currently EXIT interrupts, this may change !) 
  - ENC_TACHO_RR (PB12)
  - ENC_TACHO_RL (PB13)
  - ENC_TACHO_FR (PB14)
  - ENC_TACHO_FL (PB15)


## UART connection to NVIDIA Jetson TX2

NVIDIA Jetson TX2 Connector J17 is /dev/ttyTHS2 3.3V signalling @57600 baud
Pin 1 GND
Pin 2 CTS
Pin 3 USB 5V
Pin 4 RXD <-- STM32F4 PA2
Pin 5 TXD  --> STM32F4 PA3
Pin 6 RTS

access device without sudo: sudo usermod -a -G dialout nvidia

on NVIDIA Jetson TX2 execute several processes:
- roscore
- roslaunch rosserial_server serial.launch port:=/dev/ttyTHS2
- rostopic pub --once /nr/engine/input/actions geometry_msgs/TwistStamped  "{ header: auto, twist: { linear: { x: 0.0, y: 0.0, z: 0.0 }, angular: { x: 0.0, y: 0.0, z: 8.0 } } }" 

twist.linear.x is throttle (speed) and twist.angular.z is steering angle --> servo to the right
